/*
 * HttpServer.h
 * Created on: Dec 31, 2020
 *      Author: Wassim
 */
#ifndef MAIN_HTTPSERVER_H_
#define MAIN_HTTPSERVER_H_
#include "freertos/FreeRTOS.h"
BaseType_t StartHttpServiceProvider();
BaseType_t StopHttpServiceProvider();
#endif /* MAIN_HttpServer_H_ */
