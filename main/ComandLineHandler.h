/*
 * ComandLineHandler.h
 * from esp-idf component
 */

#ifndef MAIN_COMANDLINEHANDLER_H_
#define MAIN_COMANDLINEHANDLER_H_

#define CONSOLE_TASK_SIZE (4096)

#define CONSOLE_HIST_LINE_SIZE (100)

void console_init();

#endif /* MAIN_COMANDLINEHANDLER_H_ */
