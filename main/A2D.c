/*
 * A2D.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "A2D.h"

#include "freertos/semphr.h"
#include "esp_err.h"
#include "driver/adc.h"
#include "esp_console.h"
#include "linenoise/linenoise.h"
#include "argtable3/argtable3.h"
#include "regex.h"

#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
//#define LOG_LOCAL_LEVEL ESP_LOG_ERROR
#include "esp_log.h"

#define TAG "ADC"

#define AD_TASK_STACK_SIZE (2048)
#define AD_TASK_DELAY_MS (100)

#define BIT_START (1 << 0)
#define BIT_STOP (1 << 1)
#define BIT_GROUPS ((BIT_START) | (BIT_STOP))

#define CHANNELS (2)

typedef struct
{
	unsigned short delta;
	unsigned short hyst_min;
	unsigned short hyst_max;
} HysteresisInfo;

typedef struct
{
	unsigned short Buffer[MovingAverageBufferSize];
	unsigned char pointer;
} AverageInfo;

typedef struct
{
	volatile unsigned char running;
	unsigned int conversions;
	unsigned short raw;
	float normalized;
	HysteresisInfo moving_hysteresis;
	AverageInfo moving_average;
} ad_channel_t;

static TaskHandle_t ad_task_handler;
static ad_channel_t ad_channels;

#define MIN(a, b) ((a) < (b) ? (a) : (b))

#define MAX(a, b) ((a) > (b) ? (a) : (b))

static uint16_t UpdateHysteresis(uint16_t input)
{
	if (input > ad_channels.moving_hysteresis.hyst_max)
	{
		ad_channels.moving_hysteresis.hyst_max = MIN(
			ad_channels.moving_hysteresis.hyst_max + ad_channels.moving_hysteresis.delta,
			AD_MAX);
		ad_channels.moving_hysteresis.hyst_min = ad_channels.moving_hysteresis.hyst_max -
												 ad_channels.moving_hysteresis.delta * 2;
		return ad_channels.moving_hysteresis.hyst_max;
	}

	if (input < ad_channels.moving_hysteresis.hyst_min)
	{
		if (ad_channels.moving_hysteresis.hyst_min > AD_MIN + ad_channels.moving_hysteresis.delta)
		{
			ad_channels.moving_hysteresis.hyst_max -= ad_channels.moving_hysteresis.delta;
			ad_channels.moving_hysteresis.hyst_min -= ad_channels.moving_hysteresis.delta;
		}
		return ad_channels.moving_hysteresis.hyst_min;
	}

	return input;
}

static float UpdateAverage(uint16_t input)
{
	ad_channels.moving_average.Buffer[ad_channels.moving_average.pointer] = input;
	ad_channels.moving_average.pointer++;
	if (ad_channels.moving_average.pointer >= MovingAverageBufferSize)
		ad_channels.moving_average.pointer = 0;
	float sum = 0;
	for (unsigned int i = 0; i < MovingAverageBufferSize; i++)
		sum += ad_channels.moving_average.Buffer[i];
	return (sum / MovingAverageBufferSize);
}

static void fn_analog(void *args)
{
	ESP_LOGD(TAG, "Enter analog task");
	for (;;)
	{
		if (ad_channels.running)
		{
			ad_channels.raw = adc1_get_raw(ADC_CHANNEL_6);
			ad_channels.normalized = UpdateAverage(UpdateHysteresis(ad_channels.raw));
			ad_channels.conversions++;
		}
		vTaskDelay(pdMS_TO_TICKS(AD_TASK_DELAY_MS));
	}
}

#define CMD_AD "ad"
#define HINT_AD "hint for ad command"

#define ARG_START "start"
#define HINT_START "start A/D conversion"

#define ARG_STOP "stop"
#define HINT_STOP "stop A/D conversion"

#define ARG_SHOW "show"
#define HINT_SHOW "show A/D statement"

#define ARG_CHANNEL "channel"
#define HINT_CHANNEL "channel index"

#define ARG_HELP "help"
#define HINT_HELP "print help"

#define HELP_AD "analog base commands\n"  \
				"ad --channel 0 start \n" \
				"ad --channel 1 stop \n"  \
				"ad --channel 7 show \n"

static struct
{
	struct arg_lit *help;
	struct arg_lit *start;
	struct arg_lit *stop;
	struct arg_lit *show;
	struct arg_int *channel;
	struct arg_end *end;
} ad_args;

static int cmd_ad(int argc, char **argv)
{
	int nerrors = arg_parse(argc, argv, (void **)&ad_args);

	if (nerrors || argc == 1 || ad_args.help->count > 0)
	{
		printf(HELP_AD);
		return 0;
	}

	if (ad_args.start->count > 0 && ad_args.stop->count > 0)
	{
		printf("Start or Stop?\n");
		return 1;
	}

	if (ad_args.channel->count == 0)
	{
		printf("you have to set channel with -c or --channel from %d up to %d\n", 0, CHANNELS);
		return 1;
	}

	//uint8_t channel = ad_args.channel->ival[0];

	if (ad_args.start->count > 0)
	{
		ad_channels.running = 1;
		printf("A/D conversion starting\n");
		return 0;
	}

	if (ad_args.stop->count > 0)
	{
		ad_channels.running = 0;
		printf("A/D conversion stopping\n");
		return 0;
	}

	if (ad_args.show->count > 0)
	{
		ad_channels.running = 0;
		printf("Running:%s, conversions:%d, last raw:%d, last normal value:%f\n",
			   ad_channels.running ? "true" : "false",
			   ad_channels.conversions,
			   ad_channels.raw,
			   ad_channels.normalized);
		return 0;
	}
	return 1;
}

static void CreateCMD()
{
	ad_args.help = arg_lit0("hH", ARG_HELP, HINT_HELP);
	ad_args.start = arg_litn("sS", ARG_START, 0, 1, HINT_START);
	ad_args.stop = arg_litn("pP", ARG_STOP, 0, 1, HINT_STOP);
	ad_args.show = arg_litn("wW", ARG_SHOW, 0, 1, HINT_SHOW);
	ad_args.channel = arg_intn("cC", ARG_CHANNEL, "<n>", 0, CHANNELS, HINT_CHANNEL);
	ad_args.end = arg_end(0);

	const esp_console_cmd_t cmd = {
		.command = CMD_AD,
		.help = ARG_HELP,
		.hint = HINT_AD,
		.func = &cmd_ad};
	ESP_ERROR_CHECK(esp_console_cmd_register(&cmd));
}

void analog_init()
{
	bzero(&ad_channels, sizeof(ad_channel_t));
	adc1_config_width(ADC_WIDTH_BIT_12);
	adc1_config_channel_atten(ADC_CHANNEL_6, ADC_ATTEN_DB_6);
	ad_channels.moving_hysteresis.delta = HysteresisBufferSize;
	unsigned short raw = adc1_get_raw(ADC_CHANNEL_6);
	ad_channels.moving_hysteresis.hyst_min = raw - HysteresisBufferSize;
	ad_channels.moving_hysteresis.hyst_max = raw + HysteresisBufferSize;

	CreateCMD();

	xTaskCreate(fn_analog, "Analog", AD_TASK_STACK_SIZE, NULL, uxTaskPriorityGet(NULL), ad_task_handler);
}
