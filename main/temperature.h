/*
 * temperature.h
 * Created on: jan 03, 2020
 *      Author: Wassim
 */
#ifndef MAIN_TEMPERATURE_H_
#define MAIN_TEMPERATURE_H_

#include "freertos/FreeRTOS.h"
#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
BaseType_t temperature_init();
BaseType_t temperature_deinit();
float temperature_get(uint8_t ch, uint16_t dig_input);

#endif /* MAIN_TEMPERATURE_H_ */
