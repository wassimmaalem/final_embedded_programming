/*
 * SystemManager.h
 * Created on: jan 05, 2020
 *      Author: Wassim
 */

#ifndef MAIN_SYSTEMMANAGER_H_
#define MAIN_SYSTEMMANAGER_H_

#include <stdint.h>
#include "freertos/FreeRTOS.h"

void control_init();

BaseType_t control_deinit();

#endif /* MAIN_SYSTEMMANAGER_H_ */
