/*
 * A2D.h
 */

#ifndef MAIN_A2D_H_
#define MAIN_A2D_H_


#include <stdint.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"


#define AD_MAX (4096)
#define AD_MIN (0)
#define MovingAverageBufferSize 10
#define HysteresisBufferSize    10
void analog_init();

uint16_t analog_get(uint16_t *result, TickType_t wait_tick);


#endif /* MAIN_A2D_H_ */
